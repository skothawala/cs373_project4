package com.skkers.patterns.builderPattern.model.Packing;

/**
 * Created by ryansevilla on 4/22/17.
 */
public interface Packing {
    public String pack();
}
