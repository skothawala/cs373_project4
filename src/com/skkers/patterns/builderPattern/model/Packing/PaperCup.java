package com.skkers.patterns.builderPattern.model.Packing;

/**
 * Created by ryansevilla on 4/22/17.
 */
public class PaperCup implements Packing {

    @Override
    public String pack() {
        return "PaperCup";
    }
}
