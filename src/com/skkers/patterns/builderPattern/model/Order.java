package com.skkers.patterns.builderPattern.model;

import java.util.ArrayList;
import java.util.List;

import com.skkers.patterns.builderPattern.model.Item.Item;

/**
 * Created by ryansevilla on 4/22/17.
 */
public class Order {
    private List<Item> items = new ArrayList<Item>();

    public void addItem(Item item){
        items.add(item);
    }

    public float getCost(){
        float cost = 0.0f;

        for (Item item : items) {
            cost += item.price();
        }
        return cost;
    }

    public void showItems(){

        for (Item item : items) {
            System.out.print("Item : " + item.name());
            System.out.print(", Packing : " + item.packing().pack());
            System.out.println(", Price : " + item.price());
        }
    }


}
