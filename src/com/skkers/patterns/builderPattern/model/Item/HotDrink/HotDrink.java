package com.skkers.patterns.builderPattern.model.Item.HotDrink;

import com.skkers.patterns.builderPattern.model.Item.Item;
import com.skkers.patterns.builderPattern.model.Packing.Packing;
import com.skkers.patterns.builderPattern.model.Packing.PaperCup;

/**
 * Created by ryansevilla on 4/22/17.
 */
public abstract class HotDrink implements Item {

    @Override
    public Packing packing() {
        return new PaperCup();
    }

    @Override
    public abstract float price();
}
