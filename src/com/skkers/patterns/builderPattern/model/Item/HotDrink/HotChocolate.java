package com.skkers.patterns.builderPattern.model.Item.HotDrink;

/**
 * Created by ryansevilla on 4/22/17.
 */
public class HotChocolate extends HotDrink {

    @Override
    public float price() {
        return 3.20f;
    }

    @Override
    public String name() {
        return "Hot Chocolate";
    }
}
