package com.skkers.patterns.builderPattern.model.Item.HotDrink;

/**
 * Created by ryansevilla on 4/22/17.
 */
public class Coffee extends HotDrink {
    @Override
    public float price() {
        return 2.35f;
    }

    @Override
    public String name() {
        return "Coffee";
    }
}
