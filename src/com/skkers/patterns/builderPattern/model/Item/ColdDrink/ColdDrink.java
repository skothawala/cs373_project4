package com.skkers.patterns.builderPattern.model.Item.ColdDrink;

import com.skkers.patterns.builderPattern.model.Item.Item;
import com.skkers.patterns.builderPattern.model.Packing.Packing;
import com.skkers.patterns.builderPattern.model.Packing.PlasticCup;

/**
 * Created by ryansevilla on 4/22/17.
 */
public abstract class ColdDrink implements Item {

    @Override
    public Packing packing(){
        return new PlasticCup();
    }

    @Override
    public abstract float price();

}
