package com.skkers.patterns.builderPattern.model.Item.ColdDrink;

/**
 * Created by ryansevilla on 4/22/17.
 */
public class Smoothie extends ColdDrink {

    @Override
    public float price() {
        return 4.20f;
    }

    @Override
    public String name() {
        return "Smoothie";
    }
}
