package com.skkers.patterns.builderPattern.model.Item;

import com.skkers.patterns.builderPattern.model.Packing.Packing;

/**
 * Created by ryansevilla on 4/22/17.
 */
public interface Item {
    public String name();
    public Packing packing();
    public float price();
}
