package com.skkers.patterns.builderPattern.model;

import com.skkers.patterns.builderPattern.model.Item.ColdDrink.IcedTea;
import com.skkers.patterns.builderPattern.model.Item.ColdDrink.Smoothie;
import com.skkers.patterns.builderPattern.model.Item.HotDrink.Coffee;
import com.skkers.patterns.builderPattern.model.Item.HotDrink.HotChocolate;

/**
 * Created by ryansevilla on 4/22/17.
 */
public class OrderBuilder {
    public Order prepareHotOrder() {
        Order order = new Order();
        order.addItem(new Coffee());
        order.addItem(new HotChocolate());
        return order;
    }

    public Order prepareColdOrder() {
        Order order = new Order();
        order.addItem(new Smoothie());
        order.addItem(new IcedTea());
        return order;
    }

}
