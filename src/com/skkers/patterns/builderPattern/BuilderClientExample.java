package com.skkers.patterns.builderPattern;

import com.skkers.patterns.builderPattern.model.Order;
import com.skkers.patterns.builderPattern.model.OrderBuilder;

/**
 * Created by ryansevilla on 4/22/17.
 */
public class BuilderClientExample {
    public static void main(String[] args){

        OrderBuilder orderBuilder = new OrderBuilder();

        Order coldOrder = orderBuilder.prepareColdOrder();
        System.out.println("Cold Order");
        coldOrder.showItems();
        System.out.println("Total Cost: " + coldOrder.getCost());

        Order hotOrder = orderBuilder.prepareHotOrder();
        System.out.println("Hot Order");
        hotOrder.showItems();
        System.out.println("Total Cost: " + hotOrder.getCost());

    }
}
