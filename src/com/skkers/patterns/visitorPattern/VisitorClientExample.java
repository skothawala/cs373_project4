package com.skkers.patterns.visitorPattern;

import com.skkers.patterns.visitorPattern.model.Element.Element;
import com.skkers.patterns.visitorPattern.model.Element.RespiratorySystem.RespiratorySystem;
import com.skkers.patterns.visitorPattern.model.Visitor.RespiratorySystemBreather;
import com.skkers.patterns.visitorPattern.model.Visitor.RespiratorySystemFunctionDisplayVisitor;

public class VisitorClientExample {

	public static void main(String[] args) throws Exception {
		Element rs = new RespiratorySystem();
		rs.accept(new RespiratorySystemFunctionDisplayVisitor());
		
		System.out.println("\nNow going to display Breathing Visitor\n");
		
		rs.accept(new RespiratorySystemBreather());
	}

}
