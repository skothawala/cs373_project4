package com.skkers.patterns.visitorPattern.model.Visitor;

import com.skkers.patterns.visitorPattern.model.Element.RespiratorySystem.Lung;
import com.skkers.patterns.visitorPattern.model.Element.RespiratorySystem.Nose;
import com.skkers.patterns.visitorPattern.model.Element.RespiratorySystem.RespiratorySystem;
import com.skkers.patterns.visitorPattern.model.Element.RespiratorySystem.Trachea;

public class RespiratorySystemFunctionDisplayVisitor implements RespiratorySystemVisitor {

	
	public void visit(RespiratorySystem r) {
		System.out.println("Printing Respiratory System Functions...");
	}
	
	public void visit(Nose n) {
		System.out.println("\tThe Nose takes in the air from outside and is also an important part of the olfactory system.");
	}
	
	public void visit(Trachea t) {
		System.out.println("\tThe trachea transports the air from the nose to the lungs (with other airways in between)");
	}
	
	public void visit(Lung l){
		System.out.println("\tThe lung's main function is to exchange O2 for CO2");
	}

}
