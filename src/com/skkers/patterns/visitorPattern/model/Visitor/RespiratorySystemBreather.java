package com.skkers.patterns.visitorPattern.model.Visitor;

import com.skkers.patterns.visitorPattern.model.Element.RespiratorySystem.Lung;
import com.skkers.patterns.visitorPattern.model.Element.RespiratorySystem.Nose;
import com.skkers.patterns.visitorPattern.model.Element.RespiratorySystem.RespiratorySystem;
import com.skkers.patterns.visitorPattern.model.Element.RespiratorySystem.Trachea;

public class RespiratorySystemBreather implements RespiratorySystemVisitor {

	@Override
	public void visit(RespiratorySystem r) {
		System.out.println("Taking one breath");		
	}

	@Override
	public void visit(Nose n) {
		System.out.println("Air is coming in through the nose at a flow rate of " + n.getFlowRate());
	}

	@Override
	public void visit(Trachea t) {
		System.out.println("The trachea is warming and delivering the air to the lungs");		
	}

	@Override
	public void visit(Lung l) {
		System.out.println("The lung is exchanging CO2 for O2 and has a tidal volume of " + l.getTidalVolume());		
	}

}
