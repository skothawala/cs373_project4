package com.skkers.patterns.visitorPattern.model.Visitor;

import com.skkers.patterns.visitorPattern.model.Element.RespiratorySystem.Lung;
import com.skkers.patterns.visitorPattern.model.Element.RespiratorySystem.Nose;
import com.skkers.patterns.visitorPattern.model.Element.RespiratorySystem.RespiratorySystem;
import com.skkers.patterns.visitorPattern.model.Element.RespiratorySystem.Trachea;

public interface RespiratorySystemVisitor {

	void visit(RespiratorySystem r);
	void visit(Nose n);
	void visit(Trachea t);
	void visit(Lung l);
}
