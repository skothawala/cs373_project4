package com.skkers.patterns.visitorPattern.model.Element.RespiratorySystem;

import java.util.ArrayList;

import com.skkers.patterns.visitorPattern.model.Element.Element;
import com.skkers.patterns.visitorPattern.model.Visitor.RespiratorySystemVisitor;

public class RespiratorySystem implements Element {
	private ArrayList<Element> subOrgans;
	
	public RespiratorySystem() {
		this.subOrgans = new ArrayList<Element>();
		this.subOrgans.add(new Nose());
		this.subOrgans.add(new Trachea());
		this.subOrgans.add(new Lung());
	}
	
	@Override
	public void accept(RespiratorySystemVisitor v) throws Exception {
		v.visit(this);
		for(Element e : this.subOrgans) {
			e.accept(v);
		}
	}
	
}
