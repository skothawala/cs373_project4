package com.skkers.patterns.visitorPattern.model.Element.RespiratorySystem;

import com.skkers.patterns.visitorPattern.model.Element.Element;
import com.skkers.patterns.visitorPattern.model.Visitor.RespiratorySystemVisitor;

public class Trachea implements Element {

	@Override
	public void accept(RespiratorySystemVisitor v) throws Exception {
		v.visit(this);
	}

}
