package com.skkers.patterns.visitorPattern.model.Element.RespiratorySystem;

import com.skkers.patterns.visitorPattern.model.Element.Element;
import com.skkers.patterns.visitorPattern.model.Visitor.RespiratorySystemVisitor;

public class Lung implements Element {

	@Override
	public void accept(RespiratorySystemVisitor v) throws Exception {
		v.visit(this);
	}

	public String getTidalVolume() {
		return "0.5 L";
	}

}
