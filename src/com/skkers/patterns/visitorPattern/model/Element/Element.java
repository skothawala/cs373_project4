package com.skkers.patterns.visitorPattern.model.Element;

import com.skkers.patterns.visitorPattern.model.Visitor.RespiratorySystemVisitor;

public interface Element {
	public void accept(RespiratorySystemVisitor v) throws Exception;
}
